use crossbeam::channel::bounded;
use std::convert::TryInto;
use std::io::{ErrorKind, Read, Result};
use std::thread;

pub struct Chunker {
    output: crossbeam::channel::Receiver<Vec<u8>>,
}

fn read_next(data: &mut dyn Read, length: usize) -> Result<Option<Vec<u8>>> {
    let mut buffer = vec![0_u8; length];
    let mut bytes_read = 0;
    let mut stop = false;
    while bytes_read < length && !stop {
        let ret = data.read(&mut buffer[bytes_read..]);
        match ret {
            Ok(0) => {
                stop = true;
            }
            Ok(n) => {
                bytes_read += n;
            }
            Err(e) => {
                if e.kind() == ErrorKind::Interrupted {
                    continue;
                } else {
                    return Err(e);
                }
            }
        }
    }
    if bytes_read == 0 {
        return Ok(None);
    } else {
        buffer.truncate(bytes_read);
        Ok(Some(buffer))
    }
}

impl Chunker {
    pub fn new(queue_depth: usize, chunk_length: u64, source: impl Read + 'static + Send) -> Self {
        let mut read = Box::new(source) as Box<dyn Read + 'static + Send>;
        let return_vector_length: usize = match chunk_length.try_into() {
            Ok(x) => x,
            Err(_) => panic!(
                "Chunk_length {} is too large to possibly fit into memory",
                chunk_length
            ),
        };
        let (input, output) = bounded(queue_depth);
        thread::spawn(move || loop {
            let ret = read_next(&mut read, return_vector_length);
            match ret {
                Ok(Some(x)) => {
                    let _ = input.send(x);
                }
                Ok(None) => {
                    break;
                }
                Err(_) => {
                    break;
                }
            };
        });
        Chunker { output }
    }
}

impl Iterator for Chunker {
    type Item = Vec<u8>;
    fn next(&mut self) -> Option<Self::Item> {
        let next = self.output.recv();
        match next {
            Ok(x) => Some(x),
            Err(_) => None,
        }
    }
}
